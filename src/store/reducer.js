import img1 from '../Images/vrbox.jpeg'
import img2 from '../Images/iphone-7.jpg'
import img3 from '../Images/Galaxy-S9.png'
import img4 from '../Images/Samsung-TV.jpg'



const initialState = {
    products : [
        
        {
            id : Math.random(),
             product : [
            {
                id : Math.random(),
                title : "iphone7",
                price : 10000,
                img : img2
            } 

                        ] 
        },
    
        {
            id : Math.random(),
            product : [
            {
                id : Math.random(),
                title : "Galaxy-S9",
                price : 11500,
                img : img3
            } 

                      ]
         },
         {
            id : Math.random(),
            product : [
                {
                    id : Math.random(),
                    title : "Samsung-TV-49-In",
                    price : 9000,
                    img : img4
                } 

                      ]
        } ,
        {
            id : Math.random(),
            product : [
                {
                    id : Math.random(),
                    title : "vrbox",
                    price : 200,
                    img : img1
                } 

                        ]
        }
],
selectedproducts: [] ,
counter : 0 

}

const reducer = (state = initialState , action) => {

    if(action.type === 'PASS_DATA')
    {

        const values = Object.values(action.val) ,
        m = [],  // I use this array to pass the data which come from the store file into storage
        h = [] ;  // I use this array to pass the data into m array , I use it if the user check his products and click the button (Send To ShoppingCart) then check more products then click the button (Send To ShoppingCart) again to pass it to ShoppingCart  

      let calc = 0 ,
      counter2 = state.counter ;

    

        if(values.length !== 0 )
        {
                    for( var w=0; w<values.length; w++) // put the data which come from the store file into h array
                    {
                        if(values[w] !== '')
                        
                        {
                            h.push(values[w][0]) ;
                        }
                    }
        
        for( var a=0; a<h.length; a++) // I use this loop to pass data which come from the store file into m array
        {
  
            if( counter2 === 0) // I use this if the user click  (Send To ShoppingCart) button one time 
            {
 
                 m.push(h[a]) ;
 
            }
            else

            if(counter2 > 0  )  // i use this if the user click  (Send To ShoppingCart) button more than  one time  to add more products
            {

                for(    var r = 0; r < state.selectedproducts.length ;r++ )
                {
                    if(state.selectedproducts[r].id !== h[a].id)
                    {
                        calc = calc + 1;
                    }
                }

                if(state.selectedproducts.length === calc)
                {
                    
                    m.push( h[a]);
                }

                calc = 0;
  
            }
          
        }

        counter2 = counter2 + 1 ;               
    }

        return {
            ...state,
            selectedproducts : state.selectedproducts.concat(m) ,
            counter : counter2
        }
    }
    
    return state;
};

export default reducer;