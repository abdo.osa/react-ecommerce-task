import React, { Component } from 'react';
import { connect } from 'react-redux'
import *  as firebase from 'firebase';
import './ShoppingCart.css'



class ShoppingCart extends Component {

  state = {
    dataFromStore : this.props.selectedproductsShow
  }


  onSubmit = (event) =>
  {

    // this fuction used to pass the products into database (firebase)


    event.preventDefault()


  
const firstname = document.getElementById('FirstName').value ,
      lastname = document.getElementById('LastName').value ,
      email = document.getElementById('Email').value ,
      phoneNum = document.getElementById('phoneNumber').value ,
      CreditCardNum = document.getElementById('CreditCardNumber').value ,
      data = this.state ,
      firebaseRef2 = firebase.database().ref('/FormData'),
      formdata = {
        FirstName : firstname ,
        LastName : lastname ,
        Email : email ,
        phoneNumber : phoneNum ,
        CreditCardNumber : CreditCardNum ,
        data : data
      }; 

     

  
     firebaseRef2.push(formdata);

     console.log(this.state);

  }
 

    render() {
      return (
        <div className="ShoppingCart">
          

         

          {/* show the checked products in the  ShoppingCart */}
          {this.props.selectedproductsShow.map(data => ( 

            <div key={Math.random()} className="Box2">

          

             

               <div className="box" key={Math.random()}>
                      
               
                        <p className="title">{data.selTitle}</p>
                      <img src={data.selImg} className="img1"  alt="img" />
                      <p className="itemePrice">
                                                                
                      {data.selPrice}
                      </p>
                 
                </div>

</div>
))} 

          <form onClick={this.onSubmit}>

                First Name   <input type="text" id="FirstName" /> 
                <br/>
                Last Name  <input type="text" id="LastName" /> 
                <br/>
                Email <input type="text" id="Email" />  
                <br/>
                phone Number  <input type="text" id="phoneNumber" />
                <br/>
                Credit Card Number <input type="text" id="CreditCardNumber" /> 
                <br />
                <button>save</button>
         
          </form>

        </div>
      );
    }
  }


  const mapStateToProps = state => {
    return {
      selectedproductsShow : state.selectedproducts
    }
}
  
  export default connect(mapStateToProps) (ShoppingCart);