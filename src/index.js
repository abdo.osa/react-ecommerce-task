import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { createStore } from 'redux'
import { Provider } from 'react-redux'
import reducer from './store/reducer';

import *  as firebase from 'firebase';

 var config = {
    apiKey: "AIzaSyBJ2N6OsB-3DcMdWQS0kBfvmQICxh0xcnY",
    authDomain: "react-task-d891e.firebaseapp.com",
    databaseURL: "https://react-task-d891e.firebaseio.com",
    projectId: "react-task-d891e",
    storageBucket: "react-task-d891e.appspot.com",
    messagingSenderId: "238438935144"
  };

  firebase.initializeApp(config);





const store = createStore(reducer);

ReactDOM.render(<Provider store={store}><App /></Provider> , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
