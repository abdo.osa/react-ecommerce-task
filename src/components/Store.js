import React, { Component } from 'react';
import {Link , NavLink} from 'react-router-dom'

import './Store.css'
import { connect } from 'react-redux'



class Store extends Component {

    state = {
        
       
    }

    

    checkProduct = (event) =>
    {

        const checkedBox = document.getElementsByClassName("checkboxchecked"),
                Title = document.getElementsByClassName("title"),
                imgScr = document.getElementsByClassName("img1"),
                price = document.getElementsByClassName("itemePrice"),
                id = document.getElementsByClassName("box");
;

           
           // check if the checkbox checked or not set the data into the state to pass it to redux
                for(var i =0 ; i<checkedBox.length ; i++)
                {

                const  x = "product" ,
                        y = i ,
                        z = x + y ;

                    if(checkedBox[i].checked === true)
                    {                       
                        this.setState({
                            [z] : [{
                                id : id[i].getAttribute('data-key'),
                                selTitle : Title[i].innerHTML,
                                selImg : imgScr[i].src ,
                                selPrice : price[i].innerHTML
                            }]
                            
                        });
                      
                    }
                    else
                    {                    
                        this.setState({
                            [z] : ''
                            
                        });

                    }

                }     

    }


   


    render() {
      return (
        <div className="Store">
          <Link to="/ShoppingCart">ShoppingCart</Link> 

          <br/>
 
                  {this.props.product1show.map(data => (

                     <div  key={data.id} className="Box">
                   
                        {data.product.map(items => ( 
                            

                            <div className="box" key={items.id} data-key={items.id}>
                                <input type="checkbox" className="checkboxchecked" value="112"  onClick={ this.checkProduct} />
                                
                                 <p className="title">{items.title}</p>
                                <img src={items.img} className="img1"  alt="img" />
                                <p className="itemePrice">
                                                                          
                                {items.price}
                                
                                جنيه  
                                </p>
                                      
                            </div>

                        ))}
                    </div>
                ))}   
    
                <button type='button' className="sendDate" onClick={() =>  this.props.passData(this.state)}>Send To ShoppingCart</button>
        </div>
      );
    }
  }
  
const mapStateToProps = state => {
    return {
        product1show : state.products
    }
}

const mapDispatchToProps = dispatch => {
    return {
        passData: (datas) => dispatch({type: 'PASS_DATA' , val:datas})
    };
}



  export default connect(mapStateToProps , mapDispatchToProps) (Store);
