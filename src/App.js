import React, { Component } from 'react';
import { BrowserRouter , Route } from 'react-router-dom';
import Store from './components/Store'
import ShoppingCart from './components/ShoppingCart'


import './App.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>

      <div className="App">
        
      <Route exact path="/" component= {Store} />
      <Route  path="/ShoppingCart" component= {ShoppingCart} />

      </div>

      </BrowserRouter>
    );
  }
}

export default App;
